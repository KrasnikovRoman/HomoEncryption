/**
 * Created by Krasnikov Roman on 20.11.17.
 */
public class AuthResultResponse {
    public String status;
    public String token;

    public AuthResultResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
