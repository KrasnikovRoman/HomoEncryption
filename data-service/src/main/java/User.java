/**
 * Created by Krasnikov Roman on 20.11.17.
 */
public class User {
    public String login;
    public String password;

    public User() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
