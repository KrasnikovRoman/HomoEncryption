import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

/**
 * Class which contains all rest services.
 */
@Path("/")
public class Service {

    @Inject
    private AuthRestService authRestService;

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response authMethod(User user) {
        boolean result = authRestService.authentication(user);
        AuthResultResponse authResultResponse = new AuthResultResponse();
        if (result) {
            authResultResponse.setStatus("OK");
            authResultResponse.setToken(String.valueOf(user.hashCode()));
        } else {
            authResultResponse.setStatus("NONE");
        }


        return Response.status(Response.Status.OK).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "Origin, Content-Type, X-Auth-Token, accept")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                .header("Access-Control-Allow-Credentials", "true")
                .entity(authResultResponse).build();
    }

}
