import {Injectable} from "@angular/core";
export abstract class ConstantService {
  loginPath: string;
  getOperationsPath: string;
  getCurrentBillPath: string;
}

@Injectable()
export class ConstantServiceLight extends ConstantService {
  constructor() {
    super();
    this.loginPath = "/login";
    this.getOperationsPath = "/operations";
    this.getCurrentBillPath = "/currentBill";
  }
}
