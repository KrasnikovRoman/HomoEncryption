import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {ConstantServiceLight} from "./constant.service";
import "rxjs/add/operator/map";
import {AuthResultResponse} from "../models/auth-result-response";
import {User} from "../models/user/user";
import {Http} from "@angular/http";
import {UtilsService} from "../utils/utils";
import {Router} from "@angular/router";




@Injectable()
export class AuthenticationService {
  private token: string;

  constructor(private router: Router, private http: Http, private constantService: ConstantServiceLight) {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  login(login: string, password: string): Observable<boolean> {
    let user = new User();
    user.login = login;
    user.password = password;
    //console.log(user.login + " " + user.password);
    return this.http.post(this.constantService.loginPath, JSON.stringify(user))
      .map((response: any) => {
        let token = response.json() && response.json().token;
        console.log(token);
        if (token) {
          this.token = token;
          localStorage.setItem('currentUser', JSON.stringify({login: user.login, token: this.token}));
          return true;
        } else {
          return false;
        }
      });
  }

  logout(): void {
    this.token = null;
    localStorage.removeItem('currentUser');
    this.router.navigate(['']);
  }
}
