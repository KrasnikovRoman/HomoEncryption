import {MockBackend, MockConnection} from "@angular/http/testing";
import {BaseRequestOptions, Http, RequestMethod, Response, ResponseOptions} from "@angular/http";
import {UtilsService} from "../utils/utils";
import {StubStorage} from "../database/data.service";
import {Transaction} from "../database/transaction";

export function fakeBackendFactory(backend: MockBackend, options: BaseRequestOptions) {
  backend.connections.subscribe((connection: MockConnection) => {
    let testUser = {
      "login": "admin",
      "password": "admin"
    };

    setTimeout(() => {

      if (connection.request.url.endsWith("/login") && connection.request.method === RequestMethod.Post) {
        let params = JSON.parse(connection.request.getBody());
        console.log(params);
        if (StubStorage.isLogin(params.login, params.password)) {
          let response = JSON.parse('{' +
            '"status": "200",' +
            '"token": ' + UtilsService.getHash(params.login + params.password) + '}');
          connection.mockRespond(new Response(new ResponseOptions({
            body: response
          })));
        } else {
          let response = JSON.parse('{' +
            '"status": "401"}');
          connection.mockRespond(new Response(new ResponseOptions({
            body: response
          })));
        }
      }

      if (connection.request.url.endsWith("/operations") && connection.request.method === RequestMethod.Get) {
        let transactions: Transaction[] = StubStorage.getTransactions();
        let response = {
          transactions: transactions
        };
        connection.mockRespond(new Response(new ResponseOptions({
          body: response
        })));
      }

    }, 300);

    if (connection.request.url.endsWith("/currentBill") && connection.request.method === RequestMethod.Get) {
      let currentBill: string = StubStorage.getCurrentBill();
      let response = {
        currentBill: currentBill
      };
      connection.mockRespond(new Response(new ResponseOptions({
        body: response
      })));
    }
  });
  return new Http(backend, options);
}

export let fakeBackendProvider = {
  provide: Http,
  useFactory: fakeBackendFactory,
  deps: [MockBackend, BaseRequestOptions]
};
