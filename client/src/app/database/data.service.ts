import {Data} from "./data";
import {Transaction} from "./transaction";

export class StubStorage {
   private static stubData: Data = new Data();

    static isLogin(login: string, password: string): boolean {
     return this.stubData.getLogin() === login && this.stubData.getPassword() === password;

  }

  static getLogin(): string {
      return this.stubData.getLogin();
  }

  static setStubData(newStubData: Data) {
     this.stubData = newStubData;
  }

  static getTransactions(): Transaction[] {
      return this.stubData.getTransactions();
  }

  static setTransactions(transactions: Transaction[]) {
      this.stubData.setTransactions(transactions);
  }

  static getCurrentBill(): string {
      return this.stubData.getCurrentBill();
  }

  static setCurrentBill(bill: string) {
      this.stubData.setCurrentBill(bill);
  }

}
