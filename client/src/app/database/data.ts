import {Transaction} from "./transaction";

export class Data {
  private login: string;
  private password: string;
  private currentBill: string;
  private transactions: Transaction[];

  constructor() {
    this.login = "admin";
    this.password = "admin";
    this.currentBill = "1023";
    this.transactions = [
      {date: new Date(2017, 1, 26, 13, 51, 0), sender: "Michael", receiver: "John", amount: "456"},
      {date: new Date(2013, 5, 13, 22, 33, 0), sender: "Alisa", receiver: "Bob", amount: "740"},
      {date: new Date(2015, 11, 26, 20, 10, 0), sender: "Artem", receiver: "Roman", amount: "89"},
      {date: new Date(2011, 3, 22, 22, 22, 0), sender: "Michael", receiver: "John", amount: "456"}
    ];
  }

  getLogin(): string {
    return this.login;
  }

  getPassword(): string {
    return this.password;
  }

  getCurrentBill(): string {
    return this.currentBill;
  }

  getTransactions(): Transaction[] {
    return this.transactions;
  }

  setLogin(login: string) {
    this.login = login;
  }

  setPassword(password: string) {
    this.password = password;
  }

  setCurrentBill(currentBill: string) {
    this.currentBill = currentBill;
  }

  setTransactions(transactions: Transaction[]) {
    this.transactions = transactions;
  }
 }
