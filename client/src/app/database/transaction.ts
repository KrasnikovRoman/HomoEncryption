export class Transaction {
  date: Date;
  sender: string;
  receiver: string;
  amount: string;
}
