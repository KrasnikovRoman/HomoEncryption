import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import {RouterModule, Routes} from "@angular/router";
import {AuthGuard} from "./guards/auth.guard";
import {AuthenticationService} from "./services/authentication.service";
import {ConstantServiceLight} from "./services/constant.service";
import {MockBackend} from "@angular/http/testing";
import {BaseRequestOptions, Http, HttpModule} from "@angular/http";
import {fakeBackendProvider} from "./services/mock-backend.service";
import {MathService} from "./encryption/math.service";
import { OperationsComponent } from './components/operations/operations.component';
import {StubStorage} from "./database/data.service";
import { NewOperationComponent } from './components/new-operation/new-operation.component';


const appRoutes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard]}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    OperationsComponent,
    NewOperationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes), HttpModule
  ],
  providers: [
    AuthenticationService,
    AuthGuard,
    ConstantServiceLight,
    MockBackend,
    BaseRequestOptions,
    fakeBackendProvider,
    MathService, StubStorage
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
