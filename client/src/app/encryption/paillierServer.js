var mathService = require('./mathService.js');
module.exports = function  paillierServer(incomingMessage){
    p = 29
    q = 47
    n = 29 * 47;
    g = 19//Math.floor(Math.random()* n);
    pubKey = [n, g];

    function genPrivKey(){
        let lambda = mathService.lcm(p - 1, q - 1);
        let u = mathService.modpow(g, lambda, n*n);
        let lOfU = Math.floor((u - 1)/n);
        return [lambda, mathService.inv(lOfU % n, n)];
    }

    this.decode = function() {
        if (incomingMessage){
            let privKey = genPrivKey();
            let argL = mathService.modpow(incomingMessage, privKey[0], pubKey[0]*pubKey[0]);
            let preresult = privKey[1] * ((argL - 1) - ((argL - 1) % n)) / n;
            console.log(preresult);
            return preresult % pubKey[0];
        }
        return 0;

    }

    this.getPrivK = function(){
        return genPrivKey();
    }
    this.getPubK = function(){
        return pubKey;
    }
};