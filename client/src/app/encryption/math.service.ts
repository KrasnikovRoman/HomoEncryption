import {Injectable} from "@angular/core";

@Injectable()
export class MathService {
  constructor() {

  }

  gcd(fn, sn) {
    if (sn > fn) {
      var tmp = sn;
      sn = fn;
      fn = tmp;
    }
    while (sn) {
      var tmp = sn;
      sn = fn % sn;
      fn = tmp;
    }
    return fn;
  }

  lcm(fn, sn) {
    return fn * sn / this.gcd(fn, sn);
  }

  modpow(val, pow, mod) {
    mod = parseInt(mod);
    var s = 1;
    var v = pow;
    var c = val;
    while (v != 0) {
      var flag = 0;
      if (v % 2 == 1) {
        if (!mod)
          s = s * c;
        else
          s = (s * c) % mod;
        v = (v - 1) / 2;
        if (!mod)
          c = c * c;
        else
          c = (c * c) % mod;
        flag = 1;
      }
      else {
        v = v / 2;
      }
      if (!flag)
        if (!mod)
          c = c * c;
        else
          c = (c * c) % mod;
    }
    return s;
  }

  inv(val, mod) {
    var n = 0;
    var a = [mod];
    var b = [val];
    var p = [((mod - (mod % val))/val)];
    var q = [mod % val];
    while (q[n] != 0){
      n = n + 1;
      a[n] = b[n-1];
      b[n] = q[n-1];
      q[n] = a[n] % b[n];
      p[n] = (a[n] - q[n])/b[n];
    }
    n = n - 1;
    var fn = a[n];
    var sn = a[n+1];
    var fc = 1;
    var sc = - p[n];
    while (n > 0){
      sn = fn;
      fn = a[n-1];
      var old_fc = fc;
      fc = sc;
      sc = old_fc + fc*(- p[n-1]);
      n = n - 1;
    }
    if (fn === mod)
      return sc + ((sc < 0)? mod : 0);
    else
      return fc + ((fc < 0)? mod : 0);
  }

}
