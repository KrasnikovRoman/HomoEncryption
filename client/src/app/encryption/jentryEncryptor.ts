import {MathService} from "./math.service";
export class jentryEncriptor{
    private num = 100000;
    private privKey = 0;
    private pubKey = [];
    private sum = 0;
    private mathService;

    constructor(){
      this.mathService = new MathService();
      this.genPrivKey();
      this.genPubKey()
    }

    private genPrivKey(){
      let flag = false;
      let privKeyGen;
      while (!flag){
        privKeyGen = Math.floor(Math.random() * 1474836480000) + 5000000000000;
        if (this.mathService.gcd(privKeyGen, this.num) == 1){
          flag = true;
          this.privKey = privKeyGen;
        }
      }
    }
    private genPubKey(){
      let count = 100;
      let locsum = 0;
      let i;
      for (i = 0; i < count; i++){
        let e = Math.floor((Math.random() * Math.pow(10, 4))) % this.num;
        let mult = Math.floor((Math.random() * 4) + 2);
        let aKey = this.privKey * mult + e * this.num;
        locsum = locsum + aKey;
        this.pubKey.push(aKey);
      }
      this.sum = locsum;
    }

    public encode(text: number){
      let b = 0;
      let randval = 0;
      let i;
      for (i = 0; i < 100; i++){
        randval = Math.random();
        if (randval > 0.5){
          b = b + this.pubKey[i];
        }
      }
      return text + b
    }

    public decode(text: number){
      while (text < 0){
        text = text + this.privKey;
      }
      return ((text % this.privKey) % this.num);
    }

    public getPubKey(){
      return this.pubKey;
    }
}
