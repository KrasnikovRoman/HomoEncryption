var mathService = require('./mathService.js');
module.exports = function Jentry(){

    num = 100000;
    privKey = 0;
    pubKey = [];
    sum = 0;


    this.genPrivKey = function(){
        let flag = false;
        let privKeyGen;
        while (!flag){
            privKeyGen = Math.floor(Math.random() * 1474836480000) + 5000000000000;
            if (mathService.gcd(privKeyGen, num) == 1){
                flag = true;
                privKey = privKeyGen;
            }
        }
    };

    this.genPubKey = function () {
        let count = 100;
        let locsum = 0;
        let i;
        for (i = 0; i < count; i++){
            let e = Math.floor((Math.random() * Math.pow(10, 4))) % num;
            let mult = Math.floor((Math.random() * 4) + 2);
            let aKey = privKey * mult + e * num;
            locsum = locsum + aKey;
            pubKey.push(aKey);
        }
        sum = locsum;
    }

    this.encode = function (text) {
        let b = 0;
        let randval = 0;
        let i;
        for (i = 0; i < 100; i++){
            randval = Math.random();
            if (randval > 0.5){
                b = b + pubKey[i];
            }
        }
        return text + b
    }
    
    this.decode = function (text) {
        while (text < 0){
            text = text + privKey;
        }
        return ((text % privKey) % num);
    }


}