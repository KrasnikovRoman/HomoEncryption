var mathService = require('./mathService.js');

module.exports = class paillierClient{
    static encode(m, pubKey){
        let r = 23;//Math.floor(Math.random()* pubKey[0]);
        return ((mathService.modpow(pubKey[1], m, pubKey[0]*pubKey[0]) * mathService.modpow(r, pubKey[0], pubKey[0]*pubKey[0])) % (pubKey[0]*pubKey[0]));
    }
    static sum(firstMessage, secondMessage, pubKey){
        let mult = firstMessage * secondMessage;
        return mult % (pubKey[0] * pubKey[0]);
    }
}