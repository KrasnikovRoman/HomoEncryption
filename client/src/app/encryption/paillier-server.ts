import {MathService} from "./math.service";
export class PaillierServerService {
  private p = 97;
  private q = 101;
  private n = 97 * 101;
  private g = 19; //Math.floor(Math.random()*n);
  private incomingMessage;
  private pubKey = [this.n, this.g];

  constructor(incomingMessage: any, private mathService: MathService) {
    this.incomingMessage = incomingMessage;
  }

  private genPrivKey() {
    let lambda = this.mathService.lcm(this.p - 1, this.q - 1);
    let u = this.mathService.modpow(this.g, lambda, this.n*this.n);
    let lOfU = Math.floor((u - 1)/this.n);
    return [lambda, this.mathService.inv(lOfU % this.n, this.n)];
  }

  public decode() {
    if (this.incomingMessage){
      let privKey = this.genPrivKey();
      let argL = this.mathService.modpow(this.incomingMessage, privKey[0], this.pubKey[0]*this.pubKey[0]);
      let preresult = privKey[1] * ((argL - 1) - ((argL - 1) % this.n)) / this.n;
      console.log(preresult);
      return preresult % this.pubKey[0];
    }
    return 0;
  }

  public getPrivK() {
    return this.genPrivKey();
  }

  public getPubK() {
    return this.pubKey;
  }


}
