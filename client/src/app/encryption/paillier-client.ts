import {MathService} from "./math.service";

export class PaillierClient {
  constructor(private mathService: MathService) {

  }

  encode(m, pubKey) {
    let r = 23;//Math.floor(Math.random * pubkey[0]);
    return ((this.mathService.modpow(pubKey[1], m, pubKey[0]*pubKey[0]) * this.mathService.modpow(r, pubKey[0], pubKey[0]*pubKey[0])) % (pubKey[0]*pubKey[0]));
  }

  sum(firstMessage, secondMessage, pubKey) {
    let mult = firstMessage * secondMessage;
    return mult % (pubKey[0] * pubKey[0]);
  }
}
