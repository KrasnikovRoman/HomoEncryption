import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-operation',
  templateUrl: './new-operation.component.html',
  styleUrls: ['./new-operation.component.css']
})
export class NewOperationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  cancel(): void {
    setTimeout(function () {
      var appOperations = document.getElementById('page-1');
      var appTransactions = document.getElementById('page-2');
      appTransactions.style.display = 'none';
      appOperations.style.display = 'block';
    }, 500);
  }

}
