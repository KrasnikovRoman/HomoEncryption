import { Component, OnInit } from '@angular/core';
import {Http} from "@angular/http";
import {ConstantServiceLight} from "../../services/constant.service";
import {Transaction} from "../../database/transaction";

@Component({
  selector: 'app-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.css']
})
export class OperationsComponent implements OnInit {

  private transactions: Transaction[];
  constructor(private http: Http, private constantService: ConstantServiceLight) {

  }

  ngOnInit() {
    this.http.get(this.constantService.getOperationsPath)
      .subscribe((response: any) => {
        let transactions: Transaction[] = response.json() && response.json().transactions;
        this.transactions = transactions;
      });
  }

  loadNewOperationPage(): void {
    setTimeout(function () {
      var operations = document.getElementById('page-1');
      var newOperation = document.getElementById('page-2');
      operations.style.display = 'none';
      newOperation.style.display = 'block';
      var inputs = document.getElementsByTagName("input");
      for (var i = 0; i < inputs.length; i++) {
        inputs[i].value = "";
      }
    }, 300);
  }

}
