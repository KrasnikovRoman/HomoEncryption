import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../services/authentication.service";
import {Http} from "@angular/http";
import {ConstantServiceLight} from "../../services/constant.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private currentBill: string;

  constructor(private http: Http, private authService: AuthenticationService, private constantService: ConstantServiceLight) { }

  ngOnInit() {
    this.http.get(this.constantService.getCurrentBillPath)
      .subscribe((response: any) => {
        let currentBill: string = response.json() && response.json().currentBill;
        this.currentBill = currentBill;
      });
  }

  loadNewOperationPage(): void {
    setTimeout(function () {
      var operations = document.getElementById('page-1');
      var newOperation = document.getElementById('page-2');
      operations.style.display = 'block';
      newOperation.style.display = 'none';
      var inputs = document.getElementsByTagName("input");
      for (var i = 0; i < inputs.length; i++) {
        inputs[i].value = "";
      }
    }, 300);
  }

  logout(): void {
    this.authService.logout();
  }

}
