import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {User} from "../../models/user/user";
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthenticationService]
})
export class LoginComponent implements OnInit {
  user: User = {
    login: "",
    password: ""
  };
  //todo Добавит значок загрузки
  loading = false;
  error = '';

  constructor(private router: Router, private authService: AuthenticationService) {
  }

  login() {
    //this.router.navigate(['/home']);
    this.loading = true;
    this.authService.login(this.user.login, this.user.password)
      .subscribe(result => {
        if (result) {
          this.router.navigate(['/home']);
        } else {
          this.error = 'Login or password is incorrect';
          this.loading = false;
        }
      });
  }

  ngOnInit() {
    this.authService.logout();
    //console.log("onINIT");
  }

}
