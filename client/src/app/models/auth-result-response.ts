export class AuthResultResponse {
  status: string;
  token: string;
}
